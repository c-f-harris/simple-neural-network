# Simple Neural Network #

This is a Python script that trains a single-hidden-layer neural network to solve the handwritten digit classification problem.

### Dependencies
- numpy
- keras (for loading the dataset, but you can use whatever source you want)
- cv2 (for visualizing the images, but you can use matplotlib instead)