
import random
import numpy as np
import cv2

#################################################################################
#                         Activation Functions
#################################################################################

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_prime(x):
    return np.exp(-1*x) / (1 + np.exp(-1*x))**2

# Numerically stable way to compute the softmax function found at this
#       github page: https://github.com/JaeDukSeo/Personal_Daily_NeuralNetwork_Practice/blob/master/a_vgg/1_mini_vgg_16.py
def softmax(x):
    shiftx = x - np.max(x)
    exp = np.exp(shiftx)
    return exp/exp.sum()

# The softmax derivative eval at the ith variable with respect to the jth variable
def softmax_jacobian(X):
    n = len(X)
    J = np.zeros((n,n))
    g = softmax(X)
    for i in range(n):
        for j in range(n):
            if i == j:
                J[i,i] = g[i] - g[i]**2
            else:
                J[i,j] = -g[i]*g[j]
    return J

# This is actually not used in the training.
def softmax_jacobian_sum(X):
    size = X.shape[1]
    dim = X.shape[0]
    J = np.zeros((dim, dim))
    for s in range(size):
        j = softmax_jacobian(X[:,s])
        J += j
    return J / size

################################################################################
#                           Debug Helpers
################################################################################

# Print the shape of the matrix for trying to handle computations
#     name == string, mat is the numpy matrix
def ps(name, mat):
    print(f'{name} shape = {mat.shape}')


################################################################################
#                           Predict with weights
################################################################################

def predict(W1,B1,W2,B2, x):
    x = x.reshape(x.shape[0],1)
    z1 = np.dot( W1, x ) + B1
    a1 = sigmoid(z1)
    z2 = np.dot( W2, a1 ) + B2
    a2 = z2.copy()
    for k in range(z2.shape[1]):
        a2[:,k] = softmax(a2[:, k])
    return a2

################################################################################
#                           Train the network
################################################################################

# returns W1, B1, W2, B2
def train_network(X, Y, hidden_dim=64, epochs=100, alpha=0.75):

    input_dim = X.shape[0]
    output_dim = Y.shape[0]
    N = X.shape[1] # Number of samples

    W1 = 2*np.random.random((hidden_dim, input_dim)) - 1
    B1 = np.zeros((hidden_dim, 1))
    W2 = 2*np.random.random((output_dim, hidden_dim)) - 1
    B2 = np.zeros((output_dim, 1))

    for epoch in range(epochs):

        # Forward propagation
        z1 = np.dot( W1, X ) + B1
        a1 = sigmoid(z1)
        z2 = np.dot( W2, a1 ) + B2
        a2 = z2.copy()
        for k in range(z2.shape[1]):
            a2[:,k] = softmax(a2[:, k])

        if np.isnan(a2).any():
            print(f'\n\n\nERROR!\n\nnan at epoch {epoch}...YHAT = {a2}\n\n\n')
            exit()

        # Back propagation
        #   NOTE: this is BATCH gradient descent,
        #     so the partial derivatives are averages. Divide by # of samples.
        abs_error = Y - a2
        dJdW2 = np.dot(abs_error, a1.T) / N
        dJdB2 = abs_error/ N
        dJdB1 = np.dot(W2.T, abs_error)
        dJdB1 = np.multiply(dJdB1, sigmoid_prime(z1)) / N
        dJdW1 = np.dot(dJdB1, X.T) / N

        # Gradient descent
        W1 = W1 + alpha*dJdW1
        for k in range(dJdB1.shape[1]):
            B1 = B1 + alpha*dJdB1[:,k].reshape(B1.shape[0],1)
        W2 = W2 + alpha*dJdW2
        for k in range(dJdB2.shape[1]):
            B2 = B2 + alpha*dJdB2[:,k].reshape(B2.shape[0],1)

        if epoch % 10 == 0:
            print(f'\n\tEPOCH: {epoch}')
            print(f'\tAVG ERROR: {np.sum(np.abs(abs_error))/N}')
            print(f'\n\tRandom tests:')
            for j in range(5):
                ridx = random.randint(0,N-1)
                print(f'\tY =    {Y[:,j]} = {np.argmax(Y[:, j])}')
                _yhat = predict(W1,B1,W2,B2,X[:,j])
                print(f'\tYHAT = {_yhat.T} = {np.argmax(_yhat)}')
            print('\t====================')

    return W1,B1,W2,B2

#
