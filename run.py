

from neural_network import *
import data_preparation

trainX,trainY,testX,testY = data_preparation.get_digit_data()
trainX = trainX.T
testX = testX.T
trainY = trainY.T
testY = testY.T

print('\n\nTRAINING DATA SHAPE = ', trainX.shape)
print('TESTING DATA SHAPE =    ', testX.shape)
print('\nTRAINING Y SHAPE = ', trainY.shape)
print('TESTING Y SHAPE =  ', testY.shape, '\n\n')

x = input('TRAIN?   Enter anything to continue... ')
x = input('ARE YOU SURE?   Enter anything to continue... ')

# Even more epochs than this is probably necessary...
weights = train_network(trainX,trainY, epochs=300)
W1,B1,W2,B2 = weights

x = input('TEST?   Enter anything to continue... ')
x = input('ARE YOU SURE?   Enter anything to continue... ')

for k in range(testX.shape[1]): # for each sample
    yhat = predict(W1,B1,W2,B2,testX[:,k])
    print(f'YHAT =  {np.argmax(yhat)}')
    print(f'Y =     {np.argmax(testY[:,k])}')
    cv2.imshow('w', testX[:,k].reshape(28,28)*255)
    cv2.waitKey(1000)


################################################################################
#                      Easy, test dataset for debug
################################################################################

X = [[0,1,0], [0,1,1], [1,0,0], [0,0,0], [1,1,0], [1,0,1], [0,0,1]]
Y = [[1,0], [0,1], [1,0], [1,0], [0,1], [0,1], [1,0]]
X = np.array(X).reshape(len(X),3).T
Y = np.array(Y).T
#weights = train_network(X,Y, epochs=1000)



#
