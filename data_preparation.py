
import random
import numpy as np
from sklearn.preprocessing import OneHotEncoder
from keras.datasets import mnist
print('\nNOTE: This tensorflow backend is only for loading mnist from keras.datasets\n\n')
def get_digit_data():

    digits = mnist.load_data()
    training, testing = digits
    trainX, trainY = training
    testX, testY = testing

    trainX = trainX.reshape(trainX.shape[0], 784)
    testX = testX.reshape(testX.shape[0], 784)
    trainX = trainX/255
    testX = testX/255

    # binary encoding
    encoder = OneHotEncoder(sparse=False)
    trainY = trainY.reshape(trainY.shape[0], 1)
    testY = testY.reshape(testY.shape[0], 1)
    trainY = encoder.fit_transform(trainY)
    testY = encoder.fit_transform(testY)

    return trainX, trainY, testX, testY







#
